package com.anywayanyday.test;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import org.json.JSONArray;
import org.json.JSONObject;


public class MasterFragment extends Fragment {

    public MasterFragment() {
    }

    private static final String urlCityCod = "http://www.anywayanyday.com/_references/AirportNames/?filter=moscow&language=ru&_Serialize=JSON";
    private static final String urlRequestId = "http://api.anywayanyday.com/api/NewRequest/?Route=2409CityCodeLON&AD=1&CN=0&CS=E&Partner=testapic&_Serialize=JSON";

    private AQuery aQuery;
    private ServiceManager service;
    private ProgressBar progressBarRequest;
    private Button search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_item_master, container, false);

        progressBarRequest = (ProgressBar) rootView.findViewById(R.id.progressRequest);
        progressBarRequest.setVisibility(View.GONE);
        progressBarRequest.setProgress(0);
        aQuery = new AQuery(getActivity());

        search = (Button) rootView.findViewById(R.id.button_search);
        search.setEnabled(false);
        search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String cityCode = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("CityCode", getString(R.string.default_city));
                        progressBarRequest.setProgress(0);
                        aQuery.progress(R.id.progressMain).ajax(
                                urlRequestId.replace("CityCode", cityCode),
                                JSONObject.class, 10 * 1000, MasterFragment.this, "getRequestId");
                    }
                });

        aQuery.progress(R.id.progressMain).ajax(
                urlCityCod.replace("moscow", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("cityName",getString(R.string.default_city))),
                JSONObject.class, 10 * 60 * 1000, this, "getCode");
        this.service = new ServiceManager(getActivity(), RestService.class, new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case RestService.MSG_COUNTER:
                        progressBarRequest.setProgress(msg.arg1);
                        showDetail();
                        if (msg.arg1 >= 100) {
                            service.stop();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progressBarRequest.setVisibility(View.GONE);
                                    progressBarRequest.setProgress(0);
                                }
                            },1000);
                        }
                        break;

                    default:
                        super.handleMessage(msg);
                }
            }
        });

        return rootView;
    }

    void showDetail() {
        boolean mTwoPane = (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
        if (mTwoPane) {
            Fragment fragment = new DetailFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();

        } else {

            Intent detailIntent = new Intent(getActivity(), DetailActivity.class);

            startActivity(detailIntent);
        }
    }
    public void getCode(String url, JSONObject json, AjaxStatus status){

        if(json != null){
            AQUtility.debug("getCode", json);
            try {
                JSONArray jsonArray = json.getJSONArray("Array");
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                aQuery.id(R.id.textView).text(jsonObject.optString("CityCode",""));
                aQuery.id(R.id.button_search).enabled(true);
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("CityCode",jsonObject.optString("CityCode","")).commit();
            }
            catch (Exception e) {
                AQUtility.debug(e);
            }
        }
    }

    public void getRequestId(String url, JSONObject json, AjaxStatus status){

        if(json != null){
            AQUtility.debug("getRequestId",json);
            try {
                if ((""+json.optString("Error","null")).equals("null")) {
                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("RequestId",json.optString("Id","")).commit();
                    progressBarRequest.setVisibility(View.VISIBLE);
                    service.start();
                }
                else {
                    alert(json.optString("Error","Error"));
                }

            }
            catch (Exception e) {
                AQUtility.debug(e);
            }
        }
    }
    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(getActivity());
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);

        bld.create().show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            service.unbind();
        } catch (Throwable t) {
            AQUtility.debug("Activity", "Failed to unbind from the service");
        }
    }
}
