package com.anywayanyday.test;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.anywayanyday.test.util.SystemUiHider;
import android.annotation.TargetApi;
import android.app.Activity;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import com.androidquery.callback.LocationAjaxCallback;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class FullscreenActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private LocationAjaxCallback cb;
    private AQuery aQuery;
    private int mAccuracy = 0,iteration=0;
    private TextView status;
    public static final boolean IS_DEBUG = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);

        AQUtility.setDebug(IS_DEBUG);
        aQuery = new AQuery(this);

        status = aQuery.id(R.id.location_status).getTextView();
        cb = new LocationAjaxCallback();
        cb.weakHandler(this, "locationCb").timeout(30 * 1000).accuracy(1000).iteration(3);

        cb.async(this);

    }

    public void locationCb(String url, Location loc, AjaxStatus status){
        iteration++;
        AQUtility.debug("iteration",iteration);
        if(loc != null && loc.hasAccuracy()){
            int currentAccuracy = (int) loc.getAccuracy();
            if(mAccuracy == 0 || mAccuracy>currentAccuracy) {
                mAccuracy = currentAccuracy;
                //new best location
                PreferenceManager.getDefaultSharedPreferences(this).edit().putFloat("mLatitude",(float)loc.getLatitude()).commit();
                PreferenceManager.getDefaultSharedPreferences(this).edit().putFloat("mLongitude",(float)loc.getLongitude()).commit();
                AQUtility.debug("mAccuracy",mAccuracy);
                if (mAccuracy<100 || iteration > 1) {
                    //good accuracy or second iteration
                    getCity();
                }
            }
        }
        else {
            getCity();
        }

    }

    private void getCity() {
        if(cb != null) cb.stop();
        float lat = PreferenceManager.getDefaultSharedPreferences(this).getFloat("mLatitude",0);
        float lng = PreferenceManager.getDefaultSharedPreferences(this).getFloat("mLongitude",0);
        if (lat==0 || lng==0) {
            //TODO manual location input must be here (no access to location)
            getCityCode(null);
        }
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(lat, lng, 1);
        } catch (IOException e) {
            getCityCode(null);
        }
        if (addresses!=null && addresses.size() > 0){
            AQUtility.debug("addresses",addresses.get(0).getLocality());
            getCityCode(addresses.get(0).getLocality());
        }
        else {
            getCityCode(null);
        }
    }

    private void getCityCode(String cityName) {
        if (TextUtils.isEmpty(cityName)) {
            cityName = getString(R.string.default_city);
        }
        status.setText("" + cityName);
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("cityName",cityName).commit();
        new Handler().postDelayed(new Runnable() {

            public void run() {

                Intent i = new Intent(FullscreenActivity.this, MasterActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }


        }, 2000);
    }

    @Override
    public void onStop(){

        super.onStop();

        if(cb != null) cb.stop();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }


    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            AQUtility.debug("skip","true");
            getCity();
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
