package com.anywayanyday.test;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class MasterActivity extends FragmentActivity {

    public static boolean mTwoPane = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);

        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;
        }
    }

}
