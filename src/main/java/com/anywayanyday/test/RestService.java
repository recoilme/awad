package com.anywayanyday.test;

import android.os.Message;
import android.preference.PreferenceManager;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 08.08.13
 * Time: 14:55
 * To change this template use File | Settings | File Templates.
 */
public class RestService extends AbstractService {
    public static final int MSG_INCREMENT = 1;
    public static final int MSG_COUNTER = 2;


    private String statusUrl = "http://api.anywayanyday.com/api/RequestState/?R=ID&_Serialize=JSON";
    private AQuery aQuery;
    private String requestId = "";
    private final AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();

    @Override
    public void onStartService() {
        aQuery = new AQuery(getApplicationContext());
        requestId = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("RequestId","");
        statusUrl = statusUrl.replace("ID", requestId);
        AQUtility.debug("statusUrl", statusUrl);
        request();
    }

    void request () {
        cb.url(statusUrl).type(JSONObject.class).weakHandler(RestService.this,"callback");
        //cb.header("Connection", "Keep-Alive");
        aQuery.ajax(cb);
    }

    public void callback(String url, JSONObject json, AjaxStatus status){
        AQUtility.debug("result", json);
        int complited = json.optInt("Completed",0);
        send(Message.obtain(null, MSG_COUNTER, complited, 0));
        if (complited<100) {
            request();
        }
    }


    @Override
    public void onStopService() {

    }

    @Override
    public void onReceiveMessage(Message msg) {
        if (msg.what == MSG_INCREMENT) {

        }
    }
}

