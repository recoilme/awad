package com.anywayanyday.test;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import org.json.JSONArray;
import org.json.JSONObject;


public class DetailFragment extends Fragment {

    public DetailFragment() {
    }

    private ListView listView;
    private AQuery aQuery;
    private String faresUrl = "http://api.anywayanyday.com/api/Fares/?R=ID&V=Matrix&VB=true&L=ru&_Serialize=JSON";
    ProgressDialog dialog;
    int cacheTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_detail, container, false);
        listView = (ListView) rootView.findViewById(R.id.item_detail);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setTitle("...");

        cacheTime = 0;
        aQuery = new AQuery(getActivity());
        String requestId = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("RequestId","");
        faresUrl = faresUrl.replace("ID", requestId);
        //get from cashe
        aQuery.ajax(
                faresUrl,
                JSONObject.class, cacheTime ,this, "getFares");


        return rootView;
    }

    public void getFares(String url, JSONObject json, AjaxStatus status){
        AQUtility.debug("fares", json);
        //DummyContent.ITEMS = new ArrayList<DummyContent.DummyItem>();
        DummyContent.ITEMS.clear();
        try {
            JSONArray airlines = json.getJSONArray("Airlines");
            for (int i=0;i<airlines.length();i++) {
                JSONObject jsonObject = airlines.getJSONObject(i);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(jsonObject.optString("Name"));

                JSONArray faresFull = jsonObject.optJSONArray("FaresFull");
                if (faresFull!=null && faresFull.length()>0) {
                    jsonObject = faresFull.getJSONObject(0);
                    stringBuilder.append(" :");
                    stringBuilder.append(jsonObject.optString("TotalAmount",""));
                    stringBuilder.append(" ");
                    stringBuilder.append(jsonObject.optString("Currency",""));

                }

                DummyContent.ITEMS.add(new DummyContent.DummyItem(""+i,stringBuilder.toString()));
            }

        }
        catch (Exception e) {
            AQUtility.debug(e);
        }


        listView.setAdapter(new ArrayAdapter<DummyContent.DummyItem>(
                getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                DummyContent.ITEMS));
        if (cacheTime==0) {
            cacheTime = 60 * 1000;
            //update
            aQuery.ajax(
                    faresUrl,
                    JSONObject.class, cacheTime ,this, "getFares");
        }
    }
}
